---
title: Comments
subtitle: Ask us Questions!
comments: true
---

# Ask us Questions During the Industrial Day

A platform for the judges to ask you questions will be added before the industrial day.
